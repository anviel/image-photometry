\documentclass{article}
\usepackage{a4}
\usepackage[utf8]{inputenc}
\usepackage{units}
\usepackage{graphicx}
\usepackage{amsmath}

\setlength{\parindent}{0mm}

\begin{document}
\title{Photometric analysis of an astronomical\\imaging system}
\date{Version 1.0\\August 20th, 2020}
\author{A. Viel, Club d'Astronomie Jupiter du Roannais}
\maketitle

\tableofcontents

% --------------------------------------------
\section{Description of the source}
% --------------------------------------------
The source is characterized by its spectral type, i.e. a stellar spectral type in the MK system, associated with an effective surface temperature of an equivalent black body, as shown on table (\ref{tab:sptype}).

\begin{table}[h]
\begin{center}
\begin{tabular}{|c|r|r|}
\hline
\emph{sptype} & T [K] & BC\\
\hline
O & 20000 & -4.0\\
\hline
B & 15000 & -3.0\\
\hline
A & 9000 & -1.0\\
\hline
F & 7000 & 0.0\\
\hline
G & 5700 & 0.0\\
\hline
K & 4000 & -0.5\\
\hline
M & 3000 & -2.0\\
\hline
\end{tabular}
\end{center}
\caption{Spectral type definitions, according to \cite{kitchin}. \emph{sptype} is the MK spectral type, T is the effective surface temperature, expressed in Kelvin, and BC is the bolometric correction, in magnitude scale.}
\label{tab:sptype}
\end{table}

The spectral emissivity of the source is given by the Planck law of black body radiation \cite{milone}:
\begin{equation}
M(\lambda) = \frac{2\,\pi\,h\,c^2}{\lambda^5}(\exp(\frac{h\,c}{\lambda\,k\,T})-1)^{-1}
\end{equation}
$M(\lambda)$ is expressed in unit of Watt per square meter per unit of wavelength, and $h = \unit[6.6261\times10^{-34}]{J.s}$ is the Planck's constant, $c = \unit[2.9979\times10^8]{m/s}$ the vacuum speed of light, and $k = \unit[1.3807\times10^{-23}]{J/K}$ the Boltzmann's constant. See figure (\ref{fig:blackbody}).

\begin{figure}[hbtp]
\begin{center}
\includegraphics[width=11cm]{bbody2.png}
\end{center}
\caption{Spectral emissivity (in arbitrary units) of a black body for different temperatures \unit[4000]{K}, \unit[5700]{K} and \unit[6500]{K}.}
\label{fig:blackbody}
\end{figure}

The source is also described by the quantity of light received, i.e. the irradiance\footnote{The radiometry terminology is used here, not the astronomical photometry terminology. In the radiometric system, the \emph{irradiance} is the amount of radiated power that falls on a surface per unit area. If the measurement involves such received radiation per unit of wavelength, it is called the \emph{spectral irradiance}, see \cite{milone}.} at the top of atmosphere, which is linked to the apparent magnitude.
Two systems of magnitude can be defined:
\begin{itemize}
\item the apparent visual magnitude $m_V$
\item the apparent bolometric magnitude $m_B = m_V + \mbox{BC}$
\end{itemize}

The relation between the irradiance and the apparent bolometric magnitude is given by \cite{kitchin}, setting the reference value for a star of magnitude zero:
\begin{equation}
f_* = \unit[(2.5\times10^{-8})\times10^{-0.4\,m_B}]{W/m^2}
\label{eq:ref_mag}
\end{equation}

The extra-atmopsheric spectral irradiance is thus defined by:
\begin{equation}
f(\lambda) = f_*\,\frac{M(\lambda)}{\int M(\lambda)\,d\lambda}
\label{eq:irrad0}
\end{equation}

This weighting factor $\int M(\lambda)\,d\lambda$ is introduced since the actual (projected) area of the source is generally unknown.

% --------------------------------------------
\section{Modeling of the atmosphere}
% --------------------------------------------
If the source is at altitude $h$ above the horizon,
it is observed through an airmass of:
\begin{equation}
am = \sec z = \frac{1}{\cos z} = \frac{1}{\sin h}
\end{equation}
where $z = 90^\circ - h$ is the zenith distance.

Atmospheric extinction reduces the amount of light received.
It is expressed as the magnitude $m_h$ for a source seen at altitude $h$:
\begin{equation}
m_h = m_B + a_\lambda\,(1 + am)
\label{eq:atmo_extinct}
\end{equation}
wher $a_\lambda$ is the extinction coefficient, which depends on
wavelength, as seen on figure (\ref{fig:extinction}):
\begin{figure}[hbtp]
\begin{center}
\includegraphics[width=11cm]{extinction.png}
\end{center}
\caption{Atmospheric extinction coefficient, expressed in magnitude scale. The figure is from \cite{kitchin}.}
\label{fig:extinction}
\end{figure}

The irradiance at ground level of the source is thus obtained by 
substituting the apparent bolometric magnitude $m_B$ in (\ref{eq:ref_mag}) by $m_h$ defined by (\ref{eq:atmo_extinct}).
For the spectral irradiance, 
this leads to multiplying the expression (\ref{eq:irrad0}) by a factor that depends on the extinction coefficient and airmass:
\begin{equation}
f_h(\lambda) = 10^{-0.4\,a_\lambda\,(1 + am)}\,f(\lambda)
\end{equation}

Atmospheric turbulence also has an effect on the image formation. It is measured by its seeing $\theta_s$, expressed in unit of angle.

% --------------------------------------------
\section{Optical path}
% --------------------------------------------
The optical system is described by the following parameters:
\begin{itemize}
\item the effective focal length $F$;
\item the entrance pupil diameter $D$;
\item the obstruction $b$, the fraction of the entrance pupil area that is lost by secondary obstruction;
\item the transmission $\tau$, or the equivalent optical density $OD = -\log_{10} \tau$.
\end{itemize}

The focal ratio is:
\begin{displaymath}
{\mathcal N} = \frac{F}{D}
\end{displaymath}

Regarding the size of the source in the focal plane, two cases are to be considered:
\begin{itemize}
\item For a star-like object, the full width at half maximum (FWHM) of the source is obtained by combining the seeing with the diffraction-limited point spread function (PSF):
\begin{equation}
\mbox{FWHM} = \sqrt{\theta_s^2 + (2.44\,\frac{\lambda}{D})^2}
\end{equation}
\item For an extended object, it is directly given by its apparent size $\theta_a$
\end{itemize}
An object is considered extended if $\theta_a > \mbox{FWHM}$.
It subtends a solid angle:
\begin{equation}
\Omega = \left\{\begin{array}{ll}\frac{\pi}{4}\mbox{FWHM}^2&\hspace{.5cm}\mbox{for star-like object}\\\\\frac{\pi}{4}\theta_a^2&\hspace{.5cm}\mbox{for extended object}\end{array}\right.
\end{equation}
As a consequence, the area of the object in the focal plane is:
\begin{equation}
\Omega\,F^2
\label{eq:area_focplane}
\end{equation}
Assuming an object on the optical axis (incidence angle is zero), the amount of power that falls on the entrance pupil is
\begin{equation}
f_h(\lambda)\,\frac{\pi}{4}\,D^2
\label{eq:recv_power}
\end{equation}
The ratio of of the power (\ref{eq:recv_power}) received over the area of the object in the focal plane (\ref{eq:area_focplane}) gives the mean irradiance in the focal plane, taking into account the obstruction and the transmission \cite{schroeder}:
\begin{align}
E(\lambda) &= \frac{(1-b)\,\tau\,\frac{\pi}{4}\,D^2}{\Omega\,F^2}\,f_h(\lambda)\nonumber\\
&= \frac{\pi\,(1-b)\,\tau}{4\,\Omega\,{\mathcal N}^2}\,f_h(\lambda)
\end{align}
This expression emphasizes the effect of the focal ratio ${\mathcal N}$ on the irradiance.

% --------------------------------------------
\section{Modeling of sensor and camera}
% --------------------------------------------
The image sensor and camera is characterized by the following parameters:
\begin{itemize}
\item the width $w$ of the sensing element;
\item the pitch $p$ of a photoelement, i.e. its width;
\item the quantum efficiency $Q(\lambda)$, which depends on wavelength. This efficiency map usually shows a unique maximum value for some wavelength in the visual range, and falls to zero for short wavelength (less than \unit[300]{nm}) because of the optical depth of the substrate, and for long wavelength (less than \unit[1100]{nm}) because of the lack of energy of the incoming photons for producing electron-hole pairs;
\item the full well capacity $n_{max}$, i.e. the maximum number of electron a photocapacitor can hold at saturation;
\item the gain $G$ of the amplifier that feeds the A/D converter. This gain is usually tunable by the observer;
\item the quantization depth $m$ of the A/D converter, expressed as a number of bit, the output voltage being quantized with $2^m$ levels numbered from 0 to $2^m-1$.
\end{itemize}

The angular field of view is given by:
\begin{equation}
FOV = \frac{w}{F}
\end{equation}

The numerical plate factor relating an angle in the sky to its dimension in the image is:
\begin{equation}
\frac{d\theta}{dX} = \frac{p}{F}
\end{equation}
where $dX$ is the scale in the focal plane expressed in pixel, and $d\theta$
is the angular scale in the sky.

The sampling is the number of pixels used to sample one resolution element, defined by its FWHM:
\begin{equation}
\frac{\mbox{FWHM}}{d\theta/dX} = \frac{F\cdot\mbox{FWHM}}{p} \geq 2
\end{equation}
where the inequality expresses the Nyquist sampling condition.

According to \cite{buil}, the rate of production of photoelectrons in one photoelement is given by:
\begin{equation}
\dot{n} =  p^2\,\int \frac{E(\lambda)\,Q(\lambda)}{h\,c/\lambda}\,d\lambda
\end{equation}
where $hc/\lambda$ is, according to Planck's law, the energy of one photon at wavelength $\lambda$.

Assuming an ideal linearity, and for an integration time $t$, the output signal of the A/D converter, expressed in ADU (Analog-to-Digital Unit) is:
\begin{equation}
S_{ADU} = \left\lceil \frac{G\,\dot{n}\,t}{n_{max}}\,(2^m-1) \right\rceil
\label{eq:S_ADU}
\end{equation}
This gain is usually expressed in power decibel:
\begin{displaymath}
g_{dB} = 20\,\log_{10} G
\end{displaymath}

In the litterature (for example \cite{buil}), this is sometimes the electronic gain that is measured, in electrons per ADU. It is defined for a given integration time as:
\begin{align}
g_e &= \frac{\dot{n}\,t}{S_{ADU}}\\
&= \frac{n_{max}}{G\,(2^m-1)}
\end{align}
Measuring the electronic gain for a given value of the amplification gain $G$ is a way to measure the full well capacity $n_{max}$.

Conversely, the relation (\ref{eq:S_ADU}) can be used to compute the integration time required to reach a desired output level.

% --------------------------------------------
%\section{Appendix: }
% --------------------------------------------

\newpage
\begin{thebibliography}{9}
\bibitem{bornwolf} M. Born, E. Wolf, \emph{Principles of Optics}, 7th ed., Cambridge University Press, 1999.
\bibitem{buil} C. Buil, \emph{CCD Astronomy}, Wilmann-Bell, 1991.
\bibitem{kitchin} C. R. Kitchin, \emph{Astrophysical Techniques}, Institute of Physics Publishing, 2003.
\bibitem{milone} E.F. Milone, W.J.F. Wilson, \emph{Solar System Astrophysics}, Springer, 2008.
\bibitem{schroeder} D. J. Schroeder, \emph{Astronomical Optics}, 2nd ed., Academic Press, 2000.
\end{thebibliography}

\end{document}

