// -----------------------------------------------------------------------------
// File       : lookuptable.js
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Purpose    : A libray of look up table classes and utility functions
// Created on : 2020-AUG-15
// Modified on: 2020-AUG-18
// -----------------------------------------------------------------------------

// Search the interval number k such that x[k] <= xi < x[k+1]
// If xi is outside of [ x[0], x[x.length-1] ], constant extrapolation
// is applied
function linear_search(x, xi)
{
   var n = x.length;
   if (n < 2)
   {
      alert("number of samples in x must be at least 2!");
      return;
   }
   if (xi < x[0])
   {
      return -1;
   }
   if (xi >= x[n-1])
   {
      return n-1;
   }
   var k1 = 0;
   var k2 = n-1;
   while(k2 > k1+1)
   {
      var km = Math.floor((k1+k2)/2);
      if (xi >= x[km])
      {
         k1 = km;
      }
      else
      {
         k2 = km;
      }
   }
   return k1;
}

// Interpolate the abscissas in xi using the lookup table defined
// by arrays x and y
function interpolate(x, y, xi)
{
   var n = x.length;
   var ni = xi.length;
   var yi = new Array(ni);
   for (i = 0; i < ni; i++)
   {
      var k = linear_search(x, xi[i]);
      if (k < 0)
      {
         yi[i] = y[0];  // constant extrapolation
      }
      else if (k >= n-1)
      {
         yi[i] = y[n-1];  // constant extrapolation
      }
      else
      {  // linear interpolation
         yi[i] = y[k]*(xi[i]-x[k+1])/(x[k]-x[k+1]) + y[k+1]*(xi[i]-x[k])/(x[k+1]-x[k]);
      }
   }
   return yi;
}

// A look up table object, defined by two arrays
// x for abscissas, and y for ordinates
function LookupTable(x, y)
{
   if (x.length != y.length)
   {
      alert("lookup table error: lengths of x (" + x.length.toString()
            +") and y (" + y.length.toString() + ") do not match!");
      return;
   }
   this.x = x.slice(0);
   this.y = y.slice(0);
}
LookupTable.prototype.interpolate = function (xi)
{
   return interpolate(this.x, this.y, xi);
}
// Map the abscissas through a function f of x
LookupTable.prototype.mapx = function (f)
{
   for(k = 0; k < this.x.length; k++)
   {
      this.x[k] = f(this.x[k]);
   }
}
// Map the ordinates through a function f of y
LookupTable.prototype.mapy = function (f)
{
   for(k = 0; k < this.x.length; k++)
   {
      this.y[k] = f(this.y[k]);
   }
}
// Map the ordinates through a function f of x and y
LookupTable.prototype.map = function (f)
{
   for(k = 0; k < this.x.length; k++)
   {
      this.y[k] = f(this.x[k], this.y[k]);
   }
}
// Apply an affine transformation a*y+b to the ordinates
LookupTable.prototype.affine = function (a, b)
{
   for(k = 0; k < this.x.length; k++)
   {
      this.y[k] = a*this.y[k] + b;
   }
}
// Add another look up table lut
LookupTable.prototype.add = function (lut)
{
   var yi = interpolate(lut.x, lut.y, this.x);
   for(k = 0; k < this.x.length; k++)
   {
      this.y[k] += yi;
   }
}
// Multiply by another look up table lut
LookupTable.prototype.product = function (lut)
{
   var yi = interpolate(lut.x, lut.y, this.x);
   for(k = 0; k < this.x.length; k++)
   {
      this.y[k] *= yi[k];
   }
}
// Integrate the look up table using trapezoidal rule
LookupTable.prototype.integrate = function ()
{
   var s = 0.0;
   for(k = 0; k < this.x.length-1; k++)
   {
      s += 0.5*(this.y[k]+this.y[k+1])*(this.x[k+1]-this.x[k]);
   }
   return s;
}
// Create a new look up table. Define abscissas in the interval [x1, x2]
// regularly sampled by n points.
// Ordinates are computed by calling a function f.
function create_lookuptable(x1, x2, n, f)
{
   var x = new Array(n);
   var y = new Array(n);
   for (k = 0; k < n; k++)
   {
      x[k] = x1 + k*(x2-x1)/(n-1);
      y[k] = f(x[k]);
   }
   return new LookupTable(x, y);
}

// Utility function
function linspace(x1, x2, n)
{
   var a = new Array(n);
   for(k = 0; k < n; k++)
   {
      a[k] = x1 + (x2-x1)/(n-1);
   }
   return a;
}
