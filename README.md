Image Photometry
================

A web application for estimating the irradiance in the focal plane of an astronomical instrument, and the integration time of an electronic detector (CCD or CMOS).

This is a rough estimate, mostly because the tool is using a standard quantum efficiency map which is calibrated using only one point at maximum efficiency.

Dependencies
==
* [Plotly](https://plotly.com/javascript/)

Getting started
==
Open the `wphot_en.html` page in any web browser. It shows a form divided in four sections:
* *Source*, where you choose the spectral type, magnitude, and apparent size of the object to observe
* *Atmosphere*, where altitude of linesight and seeing are to be set
* *Optics*, where you can choose an instrument in the list, or directly set the basic optical properties like focal length, aperture diameter, transmission
* *Sensor*, where you can choose a camera and sensor in a list, or directly fill the sensor properties like width, pitch, efficiency, ...

Two buttons titled *Compute* at the top and bottom of the form yield the results.

A button titled *Plot efficiency* opens a plot of the quantum efficiency map for the wave lengths in the visual range.

A french language version of this application is available in `wphot_fr.html`.

Principle of computation
==
All details about the assumptions, formula and references are available in the `doc/photometry_image.pdf` document.

Example
==
As an example, enter the following datas:
* Spectral type: A
* Apparent magnitude: 0 in bolometric scale
* Apparent size: 0 arcmin
* Altitude of source: 45 degree
* Seeing: 2.5 arcsec
* Optics/instrument: OMP T60
* Sensor/model: OMP T60 CCD array

Click the *Compute* button.

Since the integration time to 2/3 full well is approximately 400 microsecond,
this corresponds to a photoelectron production rate of:
![photoelectron production rate](doc/photoelectron_rate_T60.png)

This is to be compared with Christian Buil's book *CCD Astronomy*, which gives a production rate of 1.8 billion electron per second for the same configuration at OMP T60.
The 7 % error gives an estimate of the accuracy of the computation.

