// -----------------------------------------------------------------------------
// File       : display_units.js
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Purpose    : Datas about astronomical instruments and image sensors
// Created on : 2020-AUG-16
// Modified on: 2022-SEP-18
// -----------------------------------------------------------------------------

function Instrument(name, flen, diam, obstruction, transmission)
{
   this.name = name;
   this.flen = flen;
   this.diam = diam;
   this.obstruction = obstruction;
   this.transmission = transmission;
}
const INSTRUMENTS = new Array(
   new Instrument("C8", 2000, 200, 22, 86),
   new Instrument("C11", 2800, 280, 22, 86),
   new Instrument("Mak 127", 1900, 127, 30, 88),
   new Instrument("Newton 130/900", 900, 130, 12, 86),
   new Instrument("Newton 150/1200", 1200, 150, 12, 86),
   new Instrument("Newton 150/750", 750, 150, 12, 86),
   new Instrument("Refractor 80ED", 500, 80, 0, 94),
   new Instrument("Refractor 150/1200", 1200, 150, 0, 92),
   new Instrument("OMP T60", 2100, 600, 8, 86)
);
function Camera(name, width, pitch, qe, max_wl, full_well, qdepth)
{
   this.name = name;
   this.width = width;
   this.pitch = pitch;
   this.qe = qe;
   this.max_wl = max_wl;
   this.full_well = full_well;
   this.qdepth = qdepth;
}
const CAMERAS = new Array(
   new Camera("Canon EOS400D", 22.3, 5.7, 50, 500, 50, 14),
   new Camera("Canon EOS600D", 22.3, 4.3, 50, 500, 25, 14),
   new Camera("ON Semi AR0130CS", 4.8, 3.8, 75, 550, 20, 12),
   new Camera("Sony ICX204", 4.5, 4.6, 45, 500 , 35, 12),
   new Camera("Sony ICX274", 8.5, 4.4, 45, 500, 35, 8),
   new Camera("Sony ICX618ALA", 4.5, 5.6, 60, 600, 50, 8),
   new Camera("Sony IMX178", 7.4, 2.4, 80, 500, 15, 14),
   new Camera("Sony IMX183", 13.2, 2.4, 83, 500, 15, 12),
   new Camera("Sony IMX290", 5.6, 2.9, 75, 500, 15, 12),
   new Camera("OMP T60 CCD array", 25.0, 21, 30, 700, 1000, 14)
);


