// -----------------------------------------------------------------------------
// File       : wphot.js
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Purpose    : Photometric analysis of an astronomical imaging system
// Created on : 2020-AUG-11
// Modified on: 2020-AUG-27
// -----------------------------------------------------------------------------

// useful constants
const BOLOMETRIC_CORRECTION = new Array(-4.0, -3.0, -1.0, 0.0, 0.0, -0.5, -2.0);
const TEMPERATURE_CLASSES = new Array(20000, 15000, 9000, 7000, 5500, 4000, 3000);
const RAD2ARCMIN = 3437.7467707849396;
const RAD2ARCSEC = 206264.80624709636;
const RAD2DEG = 57.29577951308232;
const CSTE_h = 6.62606896e-34;  // J.s
const CSTE_c = 299792458.0;  // m/s
const CSTE_k = 1.3807e-23 // J/K
// Atmospheric extinction - wavelength m
const EXTINCTION_LAM = new Array(
   300e-9,
   325.361939876e-9,
   338.442765125e-9,
   353.371423093e-9,
   372.769308544e-9,
   393.962729563e-9,
   426.735611746e-9,
   464.84934062e-9,
   509.234369689e-9,
   554.543315116e-9,
   612.329489493e-9,
   658.595037e-9,
   705.777963723e-9,
   747.646192331e-9,
   779.710883909e-9,
   800e-9);
// Atmospheric extinction - magnitude
const EXTINCTION_A = new Array(
   10.0,
   0.490772167455,
   0.426109353346,
   0.376100201344,
   0.328985696729,
   0.284789373219,
   0.239024135136,
   0.19174489449,
   0.151745417462,
   0.119072771488,
   0.0862903014931,
   0.0682791621996,
   0.0561279188348,
   0.0513584185341,
   0.0466752085349,
   10.0);
// Sensor sensitivity - wavelength m
const SENSOR_RESP_LAM = new Array(
   350.0e-9,
   375.0e-9,
   400.7873172e-9,
   450.864300579e-9,
   500.889397725e-9,
   547.999842085e-9,
   602.139743163e-9,
   654.761407922e-9,
   702.330932655e-9,
   749.855338924e-9,
   801.587041977e-9,
   850.446954786e-9,
   900.75968214e-9,
   950.405784187e-9,
   1000e-9
);
// Sensor sensitivity - efficiency %
const SENSOR_RESP_VAL = new Array(
   0.0,
   0.35,
   0.551505546751,
   0.73058637084,
   0.873217115689,
   0.968304278922,
   1.0,
   1.0,
   0.88589540412,
   0.771790808241,
   0.613312202853,
   0.437400950872,
   0.282091917591,
   0.158478605388,
   0.0
);
const SENSOR_RESP_MAX = 6;  // index of maximum sensitivity

function create_sensitivity_lut(qe_max, wl_max)
{
   var left = new LookupTable(SENSOR_RESP_LAM.slice(0,SENSOR_RESP_MAX+1),
                              SENSOR_RESP_VAL.slice(0,SENSOR_RESP_MAX+1));
   left.mapx(
      function (x)
      {
         var x0 = SENSOR_RESP_LAM[0];
         var xm = SENSOR_RESP_LAM[SENSOR_RESP_MAX];
         return x0 + (x - x0) * (wl_max - x0) / (xm - x0);
      }
   );

   var right = new LookupTable(SENSOR_RESP_LAM.slice(SENSOR_RESP_MAX+1),
                               SENSOR_RESP_VAL.slice(SENSOR_RESP_MAX+1));
   right.mapx(
      function (x)
      {
         var xm = SENSOR_RESP_LAM[SENSOR_RESP_LAM.length-1];
         var x0 = SENSOR_RESP_LAM[SENSOR_RESP_MAX+1];
         return xm - (xm - x) * (xm - wl_max) / (xm - x0);
      }
   );

   sens_lut = new LookupTable(
      left.x.concat(right.x),
      left.y.concat(right.y)
   );
   sens_lut.affine(qe_max, 0.0);
   return sens_lut;
}

function CameraList(select_list, inp_width, rad_width, inp_pitch, inp_qe, inp_max_wl, inp_full_well, inp_qdepth, inp_adu23)
{
   this.select_list = document.getElementById(select_list);
   for (k = 0; k < CAMERAS.length; k++)
   {
      this.select_list.add(new Option(CAMERAS[k].name));
   }
   this.select_list.onchange = this.select.bind(this);
   this.inp_width = document.getElementById(inp_width);
   this.rad_width = document.getElementById(rad_width);
   this.inp_pitch = document.getElementById(inp_pitch);
   this.inp_qe = document.getElementById(inp_qe);
   this.inp_max_wl = document.getElementById(inp_max_wl);
   this.inp_full_well = document.getElementById(inp_full_well);
   this.inp_qdepth = document.getElementById(inp_qdepth);
   this.inp_adu23 = document.getElementById(inp_adu23);
   // initialize
   this.select();
}
CameraList.prototype.select = function ()
{
   var sel = this.select_list.selectedIndex; 
   this.inp_width.value = CAMERAS[sel].width;
   this.rad_width.checked = true;
   this.inp_pitch.value = CAMERAS[sel].pitch;
   this.inp_qe.value = CAMERAS[sel].qe;
   this.inp_max_wl.value = CAMERAS[sel].max_wl;
   this.inp_full_well.value = CAMERAS[sel].full_well;
   this.inp_qdepth.value = CAMERAS[sel].qdepth;
   var adu23 = Math.floor(0.6666*Math.pow(2.0, CAMERAS[sel].qdepth));
   this.inp_adu23.value = adu23.toFixed(0);
}
CameraList.prototype.get_current_name = function ()
{
   var sel = this.select_list.selectedIndex; 
   return CAMERAS[sel].name;
}
CameraList.prototype.get_current_qe = function ()
{
   var sel = this.select_list.selectedIndex; 
   return CAMERAS[sel].qe;
}
CameraList.prototype.get_current_wl_max = function ()
{
   var sel = this.select_list.selectedIndex; 
   return 1e-9*CAMERAS[sel].max_wl;
}

function InstrumentList(instrument_list, inp_flen, inp_diam, inp_obstruction, inp_transmission, radio_trans)
{
   this.instrument_list = document.getElementById(instrument_list);
   this.inp_flen = document.getElementById(inp_flen);
   this.inp_diam = document.getElementById(inp_diam);
   this.inp_obstruction = document.getElementById(inp_obstruction);
   this.inp_transmission = document.getElementById(inp_transmission);
   this.radio_trans = document.getElementById(radio_trans);
   for(k = 0; k < INSTRUMENTS.length; k++)
   {
      this.instrument_list.add(new Option(INSTRUMENTS[k].name));
   }
   this.instrument_list.onchange = this.select.bind(this);
   // initialize
   this.select();
}
InstrumentList.prototype.select = function ()
{
   var sel = this.instrument_list.selectedIndex;
   this.inp_flen.value = INSTRUMENTS[sel].flen;
   this.inp_diam.value = INSTRUMENTS[sel].diam;
   this.inp_obstruction.value = INSTRUMENTS[sel].obstruction;
   this.inp_transmission.value = INSTRUMENTS[sel].transmission;
   this.radio_trans.checked = true;
}

// The main object that holds all the photometry datas
// and do the computations
function Photometry()
{
}
Photometry.prototype.set_input = function(
   spec_type, mag_val, mag_type, app_size, app_size_unit,
   alt_source, seeing,
   flen, diam, obstruction, transmission, transmission_unit,
   swidth, swidth_unit, pitch, qe_max, wl_max, full_well, qdepth, gain,
   adu23
)
{
   // Convert all units to SI, when possible
   this.spec_type = spec_type; // 0=0, 1=B, 2=A, 3=F, 4=G, 5=K, 6=M
   this.mag_val   = mag_val;   // depends on type
   this.mag_type  = mag_type;  // 1 (visual), 2 (bolometric)
   this.app_size_unit = app_size_unit;  // 1 (arcmin), 2 (arcsec)
   this.app_size = app_size;
   if (this.app_size_unit == 1)
   {
      this.app_size *= 60.0;  // arcmin to arcsec
   }
   this.app_size /= RAD2ARCSEC;  // arcsec to radian
   this.alt_source = alt_source/RAD2DEG;  // deg to radian
   this.seeing = seeing/RAD2ARCSEC;       // arcsec to radian
   this.flen = flen*0.001;  // mm to m
   this.diam = diam*0.001;  // mm to m
   this.obstruction = obstruction*0.01;         // % to [0,1]
   this.transmission_unit = transmission_unit;  // 1 (%), 2 (O.D.)
   this.transmission = transmission;
   if (this.transmission_unit == 1)
   {
      this.transmission *= 0.01;  // % to [0,1]
   }
   else if (this.transmission_unit == 2)
   {
      // optical density to transmission [0,1]
      this.transmission = Math.pow(10.0, -this.transmission);
   }
   this.pitch = pitch*1e-6;         // µm to m
   this.swidth_unit = swidth_unit;  // 1 (pixel), 2 (mm)
   if (this.swidth_unit == 1)
   {
      this.swidth = swidth*this.pitch;  // pixel to m
   }
   else if (this.swidth_unit == 2)
   {
      this.swidth = swidth*0.001;  // mm to m
   }
   this.qe_max = qe_max*0.01;       // % to [0,1]
   this.wl_max = wl_max*1e-9;       // nm to m
   this.full_well = full_well*1000; // ke- to electrons
   this.qdepth = qdepth;
   this.gain = gain;
   this.adu23 = adu23;
}
// Compute everything in a big func, to be enhanced by defining an object!
Photometry.prototype.update = function ()
{
   // Get bolometric (radiometric) magnitude
   var mag_bolo;
   if (this.mag_type == 1)
   {
      // visual mag: apply bolometric correction,
      // depending on spectral type
      mag_bolo = this.mag_val + BOLOMETRIC_CORRECTION[this.spec_type];
   }
   else if (this.mag_type == 2)
   {
      // bolometric mag is directly given
      mag_bolo = this.mag_val;
   } 
   else
   {
      alert("Invalid magnitude system!");
   }

   // Exitance of a black body, normalized
   var local_spec_type = this.spec_type;
   //var f_lambda = create_lookuptable(300e-9, 800e-9, 5,
   var f_lambda = create_lookuptable(300e-9, 800e-9, 501,
      function (lam)
      {
         return 2*Math.PI*CSTE_h*CSTE_c*CSTE_c/Math.pow(lam,5)
                / (Math.exp(CSTE_h*CSTE_c
                   / (lam*CSTE_k*TEMPERATURE_CLASSES[local_spec_type]))-1);
      });
   f_lambda.affine(1.0/f_lambda.integrate(), 0.0);

   // Irradiance, top of atmosphere
   this.irrad0 = 2.5e-8 * Math.pow(10.0, -0.4*mag_bolo);
   f_lambda.affine(this.irrad0, 0.0);

   // Airmass
   this.airmass = 1/Math.sin(this.alt_source);

   // Irradiance at ground, reduced by wavelength-dependent extinction
   var extinction = new LookupTable(EXTINCTION_LAM, EXTINCTION_A);
   var local_airmass = this.airmass;
   extinction.mapy(
      function (a)
      {
         return Math.pow(10.0, -0.4*a*(1+local_airmass));
      });
   f_lambda.product(extinction);

   // Focal ratio
   this.fratio = this.flen / this.diam;

   // FWHM: convolution of diffraction-limited PSF by seeing
   this.fwhm = Math.sqrt(Math.pow(2.44*this.wl_max/this.diam,2)
                        + Math.pow(this.seeing,2));

   // Actual solid angle
   var solid_angle;
   if (this.app_size > this.fwhm)
   {
      // solid angle of extended source
      solid_angle = 0.25*Math.PI*this.app_size*this.app_size;  // steradian
      this.obj_type = 1;  // extended
      //document.getElementById("obj_type").value = "extended";
   }
   else
   {
      // solid angle of PSF
      solid_angle = 0.25*Math.PI*this.fwhm*this.fwhm;  // steradian
      this.obj_type = 2;  // point-like
   }

   // Irradiance at focal plane
   f_lambda.affine(this.transmission*(1.0-this.obstruction)*0.25*Math.PI
                   / (this.fratio*this.fratio*solid_angle), 0.0);
   this.irrad = f_lambda.integrate();

   // Sensor properties
   var sensitivity = create_sensitivity_lut(this.qe_max, this.wl_max);

   // Field, arcmin
   this.field = this.swidth / this.flen;

   // Plate factor, arcsec/pixel
   this.plate_factor = this.pitch / this.flen;

   // Sampling pixel/FWHM
   this.sampling = this.fwhm*this.flen/this.pitch;

   // Integration time to 2/3 full well, sec
   f_lambda.product(sensitivity);
   var area = this.pitch*this.pitch;
   f_lambda.map(
      function (lam, energy)
      {
         var eplanck = CSTE_h*CSTE_c/lam;  // energy of one photon
         return energy*area/ eplanck;
      }
   );
   var tfull = this.full_well / f_lambda.integrate();
   this.t23 = 0.6666*tfull;

   // Integration time to 2/3 of max ADU, including gain
   var gain = Math.pow(10.0, this.gain/20.0);  // from dB
   this.t23_gain = this.adu23/Math.pow(2, this.qdepth) * tfull/gain;
}


function Controller(mdl)
{
   // The model to work with
   this.mdl = mdl;

   // the language
   if (document.URL.indexOf("_en.html") != -1)
   {
      this.language = "en";
   }
   else if (document.URL.indexOf("_fr.html") != -1)
   {
      this.language = "fr";
   }

   // Button callbacks
   bt_calc1.onclick = this.compute.bind(this);
   bt_calc2.onclick = this.compute.bind(this);
   bt_plot.onclick = this.cb_plot_efficiency.bind(this);

   // Create & initialize the drop-down lists
   this.camera_list = new CameraList("camera_list", "swidth", "width_mm", "pitch", "qe_max",  "wl_max", "full_well", "qdepth", "adu23");
   this.instrument_list = new InstrumentList("instrument_list", "flen", "diam", "obstruction", "transmission", "td_trans");

   // Display unit widgets
   this.irrad0 = new Unit("irradiance", 0.0, "irrad0", "irrad0_unit", 6);
   //this.fwhm = new Unit("angle", 0.0, "fwhm", "fwhm_unit", 4);
   this.irrad = new Unit("irradiance", 0.0, "irrad", "irrad_unit", 6);
   //this.field = new Unit("angle", 0.0, "field", "field_unit", 4);
   //this.plate_factor = new Unit("angle", 0.0, "plate_factor", "plate_factor_unit", 4);
   this.t23 = new Unit("time", 0.0, "t23", "t23_unit", 5);
   this.t23_gain = new Unit("time", 0.0, "t23_gain", "t23_gain_unit", 5);

   // Initialize the desired count field 
   var qdepth = parseInt(document.getElementById("qdepth").value);
   var adu23 = Math.floor(0.6666 * Math.pow(2, qdepth));
   document.getElementById("adu23").value = adu23.toFixed(0);
}
Controller.prototype.compute = function ()
{
   this.mdl.set_input(
      document.getElementById("spectral_type").selectedIndex,
      parseFloat(document.getElementById("mag_val").value),
      document.getElementById("mag_type_visual").checked ? 1 : 2,
      parseFloat(document.getElementById("app_size").value),
      document.getElementById("size_unit_arcmin").checked ? 1 : 2,
      parseFloat(document.getElementById("alt_source").value),
      parseFloat(document.getElementById("seeing").value),
      parseFloat(document.getElementById("flen").value),
      parseFloat(document.getElementById("diam").value),
      parseFloat(document.getElementById("obstruction").value),
      parseFloat(document.getElementById("transmission").value),
      document.getElementById("td_trans").checked ? 1 : 2,
      parseFloat(document.getElementById("swidth").value),
      document.getElementById("width_pixel").checked ? 1 : 2,
      parseFloat(document.getElementById("pitch").value),
      parseFloat(document.getElementById("qe_max").value),
      parseFloat(document.getElementById("wl_max").value),
      parseInt(document.getElementById("full_well").value),
      parseInt(document.getElementById("qdepth").value),
      parseFloat(document.getElementById("gain").value),
      parseInt(document.getElementById("adu23").value)
   );
   this.mdl.update();
   this.update_view();
}
Controller.prototype.update_view = function ()
{
   this.irrad0.set_value(this.mdl.irrad0);
   if (this.language == "en")
   {
      document.getElementById("obj_type").value = this.mdl.obj_type == 1 ? "extended" : "star-like";
   }
   else if (this.language == "fr")
   {
      document.getElementById("obj_type").value = this.mdl.obj_type == 1 ? "étendu" : "ponctuel";
   }
   document.getElementById("airmass").value = this.mdl.airmass.toPrecision(3);
   document.getElementById("fratio").value = this.mdl.fratio.toFixed(1);
   document.getElementById("fwhm").value = (this.mdl.fwhm*RAD2ARCSEC).toPrecision(4);
   this.irrad.set_value(this.mdl.irrad);
   document.getElementById("field").value = (this.mdl.field*RAD2ARCMIN).toPrecision(4);
   document.getElementById("plate_factor").value = (this.mdl.plate_factor*RAD2ARCSEC).toPrecision(4);
   document.getElementById("sampling").value = this.mdl.sampling.toPrecision(3);
   this.t23.set_value(this.mdl.t23);
   this.t23_gain.set_value(this.mdl.t23_gain);
}
Controller.prototype.cb_plot_efficiency = function ()
{
   // Build quantum efficiency look up table
   qe_max = parseFloat(document.getElementById("qe_max").value);
   wl_max = 1e-9 * parseFloat(document.getElementById("wl_max").value);
   lut = create_sensitivity_lut(qe_max, wl_max);

   // Pack it into a Plotly object
   scr1 = "var trace = { x: [";
   for (k = 0; k < lut.x.length; k++)
   {
      scr1 += (1e9*lut.x[k]).toFixed(0);
      if (k < lut.x.length-1)
      {
         scr1 += ",";
      }
   }
   scr1 += "], y: [";
   for (k = 0; k < lut.y.length; k++)
   {
      scr1 += (lut.y[k]).toString();
      if (k < lut.y.length-1)
      {
         scr1 += ",";
      }
   }
   var title = "Quantum efficiency of ";
   var xlabel = "wavelength [nm]";
   var ylabel = "quantum efficiency [%]";
   if (this.language == "fr")
   {
      title = "Rendement quantique ";
      xlabel = "longueur d\'onde [nm]";
      ylabel = "rendement quantique [%]";
   }
   scr1 += "], type: \"scatter\" }; var layout = { xaxis: { title: \""+xlabel+"\" }, yaxis: { title: \""+ylabel+"\" } }; Plotly.newPlot(\"plot_div\", [trace], layout);"

   // Prepare HTML content of new window
   txt = "<!DOCTYPE html> <html> <head> <script type=\"text/javascript\" src=\"plotly-latest.min.js\"></script> <style type=\"text/css\"> h2 { text-align: center; color: yellow } </style></head> <body bgcolor=\"#bbb0a1\"> <h2>"+title+this.camera_list.get_current_name()+"</h2> <div id=\"plot_div\"></div><script type=\"text/javascript\">"+scr1+"</script> </body> </html>";

   // Create and display new window
   plot_win = window.open("", "plot Q.E.", "close,location=false,width=800,height=600");
   plot_win.document.write(txt);
   plot_win.document.close();
}

function init()
{
   document.controller = new Controller(new Photometry);
}
window.onload = init;

