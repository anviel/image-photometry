// -----------------------------------------------------------------------------
// File       : display_units.js
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Purpose    : A display unit widget
// Created on : 2020-AUG-13
// Modified on: 2020-AUG-27
// -----------------------------------------------------------------------------

const UNITS = { 
   "angle": [ [ "degree", 1 ], [ "arcmin", 60 ], [ "arcsec", 3600 ], [ "radian", 0.01745329 ] ],
   "irradiance": [ [ "W/m**2", 1 ], [ "kW/m**2", 0.001 ], [ "mW/m**2", 1000 ], [ "uW/m**2", 1000000 ], [ "W/cm**2", 0.0001 ], [ "mW/cm**2", 0.1 ], [ "uW/cm**2", 100] ],
   "length": [ ["m", 1 ], ["cm", 100], ["mm", 1000], [ "um", 1000000 ], [ "nm", 1000000000 ] ],
   "time": [ [ "sec", 1 ], [ "min", 1/60.0 ], [ "hr", 1/3600.0 ], [ "d", 1/86400.0 ], [ "millisec", 1000 ], [ "microsec", 1000000 ], [ "nanosec", 1000000000 ] ]
}

function Unit(unit_name, value, text_box, unit_menu, precision)
{
   this.unit_name = unit_name;
   this.text_box = document.getElementById(text_box);
   this.unit_menu = document.getElementById(unit_menu);
   for (k = 0; k < UNITS[unit_name].length; k++)
   {
      this.unit_menu.add(new Option(UNITS[unit_name][k][0]));
   }
   this.unit_menu.onchange = this.change_unit.bind(this);
   this.precision = precision;
   this.set_value(value);
}
Unit.prototype.set_value = function(v)
{
   this.value = v;
   var sel = this.unit_menu.selectedIndex;
   var disp_val = v * UNITS[this.unit_name][sel][1];
   this.text_box.value = disp_val.toPrecision(this.precision);
}
Unit.prototype.change_unit = function()
{
   var sel = this.unit_menu.selectedIndex;
   var disp_val = this.value * UNITS[this.unit_name][sel][1];
   this.text_box.value = disp_val.toPrecision(this.precision);
}

